					<div id="copyright">
						<ul><li><a href="http://osgeo.asso.fr/" title="Rejoindre le site de l'OSGeo-fr"><img src ="./images/logo-osgeo.png" style="width:150px;" /></a></li><li>&copy; OSGeo-fr</li><li>Email : <a href="mailto:foss4gfr@osgeo.asso.fr">foss4gfr@osgeo.asso.fr</a></li></ul>
						<p>Site réalisé par <a href="http://www.intermezzo-coop.eu" title="Simon Georget">SG</a> sur la base d'un design <a href="https://html5up.net">HTML5 UP</a></p>
					</div>
					
					<!-- Return to Top -->
					<a href="#wrapper" id="return-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
					
					
					<!-- Piwik -->
					<script type="text/javascript">
					  var _paq = _paq || [];
					  _paq.push(["setDomains", ["*.osgeo.asso.fr/foss4gfr-2018"]]);
					  _paq.push(['trackPageView']);
					  _paq.push(['enableLinkTracking']);
					  (function() {
						var u="//osgeo.asso.fr/piwik/";
						_paq.push(['setTrackerUrl', u+'piwik.php']);
						_paq.push(['setSiteId', '5']);
						var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
						g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
					  })();
					</script>
					<noscript><p><img src="//osgeo.asso.fr/piwik/piwik.php?idsite=5" style="border:0;" alt="" /></p></noscript>
					<!-- End Piwik Code -->

					
					