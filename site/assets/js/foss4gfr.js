
						
						jQuery(document).ready(function($) {
							// open external links in new window
							$('a').each(function() {
							   var a = new RegExp('/' + window.location.host + '/');
							   if(!a.test(this.href)) {
								   $(this).click(function(event) {
									   event.preventDefault();
									   event.stopPropagation();
									   window.open(this.href, '_blank');
								   });
							   }
							});
							
							// smooth scroll
							$(document).on('click', 'a[href^="#"]', function (event) {
								event.preventDefault();

								$('html, body').animate({
									scrollTop: $($.attr(this, 'href')).offset().top
								}, 500);
							});
							
							
							// ===== Scroll to Top ==== 
							$(window).scroll(function() {
								if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
									$('#return-to-top').fadeIn(200);    // Fade in the arrow
								} else {
									$('#return-to-top').fadeOut(200);   // Else fade out the arrow
								}
							});
							/**
							$('#return-to-top').click(function() {      // When arrow is clicked
								$('body,html').animate({
									scrollTop : 0                       // Scroll to top of body
								}, 500);
							});
							* */
							
						});
